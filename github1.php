<?php
require_once 'vendor/autoload.php';
use Guzzle\Http\Client;
use Guzzle\Log\MonologLogAdapter;
use Guzzle\Plugin\Log\LogPlugin;
use Guzzle\Log\MessageFormatter;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// Create a client and provide a base URL
$client = new Client('https://api.github.com');
$log = new Logger('log');
$log->pushHandler(new StreamHandler(__DIR__.'/log/github.log'));

$adapter = new MonologLogAdapter($log);

$logPlugin = new LogPlugin($adapter, MessageFormatter::DEBUG_FORMAT);

$client->addSubscriber($logPlugin);

// Create a request with basic Auth
$request = $client->get('/user')->setAuth('user', 'password');
// Send the request and get the response
$response = $request->send();
print_r(json_decode($response->getBody()));
// >>> {"type":"User", ...
echo $response->getHeader('Content-Length');
// >>> 792
