Teach Guzzle and Monolog
====================

### Links:

-   [http://docs.guzzlephp.org/en/latest/](http://docs.guzzlephp.org/en/latest/).
-   [https://github.com/guzzle/guzzle](https://github.com/guzzle/guzzle).
-   [http://www.sitepoint.com/guzzle-php-http-client/](http://www.sitepoint.com/guzzle-php-http-client/).
