<?php
/**
 * User: duff
 * Date: 03.02.14
 * Time: 16:36
 */
ini_set('display_errors', 1);

require_once 'vendor/autoload.php';

use Guzzle\Http\Client;
use Guzzle\Log\MonologLogAdapter;
use Guzzle\Plugin\Log\LogPlugin;
use Guzzle\Log\MessageFormatter;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$client = new Client('https://api.vk.com/');


$log = new Logger('log');
$log->pushHandler(new StreamHandler(__DIR__.'/log/vk.log'));

$adapter = new MonologLogAdapter($log);

$logPlugin = new LogPlugin($adapter, MessageFormatter::DEBUG_FORMAT);

$client->addSubscriber($logPlugin);

$request = $client->get('method/wall.get');
$query = $request->getQuery();
$query->add('owner_id', 157545862);
$request->getCurlOptions()->set(CURLOPT_SSL_VERIFYPEER, false);
try {

    $response = $request->send();
    print_r(json_decode($response->getBody(true)));
} catch (Exception $e) {
    echo $request->getResponse()->getRawHeaders();
}