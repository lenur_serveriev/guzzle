<?php
ini_set('display_errors', 1);

require_once 'vendor/autoload.php';

use Guzzle\Http\Client;
use Guzzle\Log\MonologLogAdapter;
use Guzzle\Plugin\Log\LogPlugin;
use Guzzle\Log\MessageFormatter;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$client = new Client('https://api.github.com');

$log = new Logger('log');
$log->pushHandler(new StreamHandler(__DIR__.'/log/github.log'));

$adapter = new MonologLogAdapter($log);

$logPlugin = new LogPlugin($adapter, MessageFormatter::DEBUG_FORMAT);

$client->addSubscriber($logPlugin);

$request = $client->post('authorizations', array(),
    json_encode(
        array(
            'scopes' => array(
                'public_repo',
                'user',
                'repo',
                'gist'
            ),
            'note' => 'auto client' . uniqid()
        )));

$request->setAuth('login', 'password');

$request->getCurlOptions()->set(CURLOPT_SSL_VERIFYPEER, false);

$response = $request->send();

$body = json_decode($response->getBody(true));
$oauthToken = $body->token;

$request = $client->get('user/keys');

$query = $request->getQuery();

$query->add('access_token', $oauthToken);

$request->getCurlOptions()->set(CURLOPT_SSL_VERIFYPEER, false);

try {

    $response = $request->send();

    print_r(json_decode($response->getBody(true)));
} catch (Exception $e) {
    echo $request->getResponse()->getRawHeaders();
}