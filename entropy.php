<?php
ini_set('display_errors', 1);
/**
 * User: duff
 * Date: 02.02.14
 * Time: 23:34
 *
 * @link http://www.sitepoint.com/guzzle-php-http-client/
 * @link http://docs.guzzlephp.org/
 */
chdir(dirname(__DIR__));
require_once "vendor/autoload.php";


use Guzzle\Http\Client;
//use Guzzle\Http\EntityBody;
//use Guzzle\Http\Message\Request;
//use Guzzle\Http\Message\Response;


use Guzzle\Log\MessageFormatter;
use Guzzle\Log\MonologLogAdapter;
use Guzzle\Plugin\Log\LogPlugin;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$logger = new Logger('client');
$logger->pushHandler(new StreamHandler(__DIR__.'/log/entropy.log'));

$logAdapter = new MonologLogAdapter($logger);

$logPlugin = new LogPlugin($logAdapter, MessageFormatter::DEBUG_FORMAT);

/** @var $client Client */

$client = new Client("https://qrng.anu.edu.au");
$request = $client->get('/API/jsonI.php?length=10&type=uint8');
$client->addSubscriber($logPlugin);

//my local  proplem with ssl sertification https://github.com/guzzle/guzzle/issues/394
//$client->setDefaultOption('verify', false);
try {
    $response = $request->send();
    $body = $response->getBody(true);
    echo $body;
} catch (Exception $e) {
    echo $request->getResponseBody();
}

